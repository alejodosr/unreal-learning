import rospy
import cv2
import numpy as np
# from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import tf
import os

class ImageFromRosbag(object):
    def __init__(self):

        self.IMAGE_TOPIC = "/camera1/image_raw"
        self.FOLDER_INIT = 0
        self.BASE_FOLDER = "/media/alejandro/DATA/datasets/simple_uav_dataset/real/"
        self.SHOW_IMAGE = True

        self.bridge = CvBridge()

        # Call the subscriber
        self.image_subcriber()


    def image_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        try:

            K = np.array([[-1726862.3903241267, 0., 160.5],
                          [0., -1726862.3903241267, 120.5],
                          [0., 0., 1.]])

            # zero distortion coefficients work well for this image
            D = np.array([0., 0., 0., 0.])

            # use Knew to scale the output
            Knew = K.copy()
            Knew[(0, 1), (0, 1)] = 1.0 * Knew[(0, 1), (0, 1)]

            cv_image = cv2.fisheye.undistortImage(cv_image, K, D=D, Knew=Knew)

            if self.SHOW_IMAGE:
                cv2.imshow("Image window", cv_image)
                cv2.waitKey(3)

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

    def image_subcriber(self):
        # In ROS, nodes are uniquely named. If two nodes with the same
        # node are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        rospy.init_node('image_subcriber', anonymous=True)

        rospy.Subscriber(self.IMAGE_TOPIC, Image, self.image_callback)

        rospy.spin()

if __name__ == '__main__':
    ImageFromRosbag()