import os
import re

# Rootdir of the dataset
ROOTDIR = "/media/alejandro/DATA/datasets/simple_uav_dataset/simulation_no_seg"
	
# Go across all files and subfolders
for subdir, dirs, files in os.walk(ROOTDIR):
    for file in files:
        if file.endswith(".txt"):
            f = open(subdir + "/" + file, 'r+')
            # Print first line
            print f.readline()
            print subdir

        if file.endswith(".jpg") or file.endswith(".JPG"):
            # Read line
            line = f.readline()
            # print(line)
            line = line.split(",")
            # print(line)

            if (int(line[0]) != 0 and int(line[1]) != 0 and int(line[2]) != 0 and int(line[3]) != 0):
                # Write info in a file
                with open(ROOTDIR + '/annotations.txt', 'a') as the_file:
                    the_file.write(str(subdir + "/" + file) + "," + str(line[0]) + "," + str(line[1]) + "," + str(int(line[0]) + int(line[2])) + "," + str(int(line[1]) + int(line[3])) + "," + "drone" + '\n')