from __future__ import print_function

import numpy as np
import cv2
import os

import roslib
# roslib.load_manifest('my_package')
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

# Rootdir of the dataset
ROOTVIDEO = "/home/alejandro/py_workspace/keras-retinanet/examples/uav_inspection.mp4"
WAIT_TIME = 333
ROS_PUB_FREQ = 20
SHOW_IMAGE_IN_OPENCV = 0

# Read from video
cap = cv2.VideoCapture(ROOTVIDEO)

# Init ros node
rospy.init_node('image_publisher', anonymous=True)

# Check if camera opened successfully
if (cap.isOpened()== False):
  print("Error opening video stream or file")

# Define image publisher
image_pub = rospy.Publisher("image", Image, queue_size=1)
bridge = CvBridge()

# Counter
counter = 0

# Capture frame-by-frame
ret, frame = cap.read()

# Ros rate
r = rospy.Rate(ROS_PUB_FREQ) # 10hz

while not rospy.is_shutdown():
    # Publish frame
    image_pub.publish(bridge.cv2_to_imgmsg(frame, "bgr8"))

    # Wait
    if SHOW_IMAGE_IN_OPENCV:
        cv2.imshow("Image window", frame)
        cv2.waitKey(WAIT_TIME)

    # Capture frame-by-frame
    ret, frame = cap.read()

    r.sleep()


# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()