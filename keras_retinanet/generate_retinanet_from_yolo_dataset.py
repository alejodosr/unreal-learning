import numpy as np
import cv2
import os
import re
import math
import csv

# Rootdir of the dataset
#rootdir = "/media/alejandro/DATA/datasets/simdronet"
ROOTDIR = '/media/alejandro/DATA/datasets/real_uav_dataset'

SHOW_IMAGES = 0
WRITE_ANNOTATIONS = 1
GENERATE_FOR_COLAB = 1

LABELS_TO_NAMES = {0: 'drone', 1: 'bird', 2: 'car'}

with open(ROOTDIR + '/data/train.txt', 'rb') as f:
    gt_list = f.readlines()

    for row in gt_list:
        # Remove carrier end
        row = row.replace('\n', '')

        # Print some info
        print(ROOTDIR)
        print(row)

        # Load an color image in grayscale
        img = cv2.imread(ROOTDIR + '/' + row)

        # Remove extension
        outfile = row.replace('.jpg', '')
        outfile = outfile.replace('.jpeg', '')
        outfile = outfile.replace('.JPG', '')
        outfile = outfile.replace('.JPEG', '')
        outfile = outfile.replace('.png', '')
        outfile = outfile.replace('.GIF', '')
        outfile = outfile.replace('.gif', '')

        height, width, channels = img.shape
        print(img.shape)

        print(ROOTDIR)
        print(row)

        # Open file
        with open(ROOTDIR + '/' + outfile + '.txt', 'rb') as f:
            gt_bb = f.readlines()

            for row2 in gt_bb:
                # Split between spaces
                row2 = row2.split()

                # Get bb
                label = int(row2[0])
                w = int(float(width) * float(row2[3]))
                h = int(float(height) * float(row2[4]))
                x = int((float(width) * float(row2[1])) - (w / 2))
                y = int((float(height) * float(row2[2])) - (h / 2))

                # Print label
                print(LABELS_TO_NAMES[label])

                # Plot rectangle
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

                if WRITE_ANNOTATIONS and w != 0 and h != 0:
                    # Write info in a file
                    with open(ROOTDIR + '/colab_annotations.txt', 'a') as the_file:
                        the_file.write(str(row) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
                                       + str(y + h) + "," + "drone" + '\n')


            if SHOW_IMAGES:
                # Show iamge
                cv2.imshow('image', img)
                cv2.waitKey(0)

                #input("Press Enter to continue...")
