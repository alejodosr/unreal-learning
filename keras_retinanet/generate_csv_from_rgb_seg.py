import cv2
import os
import re
import math
import numpy as np

# Rootdir of the dataset
#rootdir = "/media/alejandro/DATA/datasets/simdronet"
ROOTDIR = "/media/alejandro/DATA/datasets/anyverse_m100_reduced_116"

SHOW_IMAGES = 0
WRITE_ANNOTATIONS = 1
GENERATE_FOR_COLAB = 1
FILTER_BY_COLOR = 1

# FOLDERS_AND_CATS = {"drone": [0, 1, 2, 3, 4, 5, 6, 7], "bird": [8], "car": [9]}
FOLDERS_AND_CATS = {"drone": [0, 1, 3]}
FORMAT = ".png"
COLOR_LOW = np.array([199, 109, 109])
COLOR_UP = np.array([201, 111, 111])
# NUMBER_OF_DECIMALS = 8
NUMBER_OF_DECIMALS = 8


# Go across all files and subfolders
for subdir, dirs, files in os.walk(ROOTDIR):
    for file in files:
        if file.endswith(FORMAT) and not file.startswith("frame_seg"):
            # Print some info
            print("Raw filename: " + file)
            result = re.findall("\d+", file)
            result = result[0]
            file_seg = "frame_seg_" + result + FORMAT
            print(file_seg)

            # Load an color image in grayscale
            img = cv2.imread(os.path.join(subdir, file))
            # Load an color image in grayscale
            img_seg = cv2.imread(os.path.join(subdir, file_seg))
            if FILTER_BY_COLOR:
                mask = cv2.inRange(img_seg, COLOR_LOW, COLOR_UP)
            else:
                mask = cv2.cvtColor(img_seg, cv2.COLOR_BGR2GRAY)
            ret, mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)
            mask /= 255
            # img = cv2.bitwise_and(img, img, mask=mask)

            x, y, w, h = cv2.boundingRect(mask)
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

            if WRITE_ANNOTATIONS and w != 0 and h != 0:
                anno_number = int(subdir[len(subdir) - 8])

                for key, value in FOLDERS_AND_CATS.items():
                    if anno_number in value:
                        annotation = key
                        break

                if not GENERATE_FOR_COLAB:
                    # Write info in a file
                    with open(subdir + '/../../annotations.txt', 'a') as the_file:
                        the_file.write(str(subdir + "/" + file) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
                                       + str(y + h) + "," + annotation + '\n')
                else:
                    # Write info in a file
                    temp = subdir[len(subdir) - 8 :len(subdir)]
                    with open(subdir + '/../../annotations.txt', 'a') as the_file:
                        the_file.write(str(temp + "/" + file) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
                                       + str(y + h) + "," + annotation + '\n')

            if SHOW_IMAGES:
                # Show iamge
                cv2.imshow('image', img)
                cv2.imshow('image_seg', img_seg)
                cv2.waitKey(0)

            #input("Press Enter to continue...")
