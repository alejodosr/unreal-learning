import cv2
import os
import re
import math

from pycocotools.coco import COCO
import numpy as np
import skimage.io as io
import matplotlib.pyplot as plt
import pylab

ANNOTATIONS_FILE = "/media/alejandro/DATA/datasets/mscoco/train2017/annotations/instances_train2017.json"
CATEGORY_NAME = ['car']
OUTDIR = "/media/alejandro/DATA/datasets/simple_m100_dataset/"
TOTAL_NO_IMAGES = 2000

FOLDER_INIT = 5

# initialize COCO api for instance annotations
coco = COCO(ANNOTATIONS_FILE)

# Load categories
cats = coco.loadCats(coco.getCatIds())
nms=[cat['name'] for cat in cats]
print('COCO categories: \n{}\n'.format(' '.join(nms)))
print('Number of COCO categories: ' + str(len(nms)))

nms = set([cat['supercategory'] for cat in cats])
print('COCO supercategories: \n{}'.format(' '.join(nms)))

catIds = coco.getCatIds(catNms=CATEGORY_NAME)
imgIds = coco.getImgIds(catIds=catIds )
# imgIds = coco.getImgIds(imgIds = [324158])

# Frame counter
frame_counter = 0
# folder_counter_prev = watcher.folder_counter
folder_counter = FOLDER_INIT

# Create folder
directory = OUTDIR + str(folder_counter)
if not os.path.exists(directory):
    os.makedirs(directory)

directory = OUTDIR + str(folder_counter) + '/images'
if not os.path.exists(directory):
    os.makedirs(directory)

while (frame_counter <= TOTAL_NO_IMAGES):
    img = coco.loadImgs(imgIds[np.random.randint(0, len(imgIds))])[0]

    annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds, iscrowd=None)
    anns = coco.loadAnns(annIds)
    mask = coco.annToMask(anns[0])
    for i in range(len(anns)):
        mask += coco.annToMask(anns[i])

    mask = np.array(mask)

    I = io.imread(img['coco_url'])
    img_bgr = np.array(I)
    try:
        img_bgr = img_bgr[:, :, ::-1].copy()
    except:
        continue

    ## check opencv version and construct the detector
    params = cv2.SimpleBlobDetector_Params()
    # thresholds
    params.minThreshold = 40
    params.maxThreshold = 200
    # params.thresholdStep = 20

    # filter by area
    params.filterByArea = True
    params.minArea = 4
    params.maxArea = 10000000

    # filter by circularity
    params.filterByCircularity = False

    # filter by convexity
    params.filterByConvexity = False

    # filter by inertia
    params.filterByInertia = False
    is_v2 = cv2.__version__.startswith("2.")
    if is_v2:
        detector = cv2.SimpleBlobDetector(params)
    else:
        detector = cv2.SimpleBlobDetector_create(params)

    ret, mask2 = cv2.threshold(mask * 255, 127, 255, cv2.THRESH_BINARY_INV)

    # Detect blobs.
    keypoints = detector.detect(mask2)

    mask_bgr = mask * 255

    # print(keypoints)

    # Only one category
    if (len(keypoints) == 1):
        # Save image
        cv2.imwrite(OUTDIR + str(folder_counter) + '/images/frame_rgb_' + str(frame_counter).zfill(6) + '.jpg',
                    img_bgr)
        cv2.imwrite(OUTDIR + str(folder_counter) + '/images/frame_seg_' + str(frame_counter).zfill(6) + '.jpg',
                    mask_bgr)
        frame_counter += 1

        x, y, w, h = cv2.boundingRect(mask)
        cv2.rectangle(img_bgr, (x, y), (x + w, y + h), (0, 255, 0), 2)

    img_bgr = cv2.drawKeypoints(img_bgr, keypoints, np.array([]), (0, 0, 255),
                                          cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    cv2.imshow("coco_image", img_bgr)
    cv2.imshow("mask_image", mask_bgr)
    cv2.waitKey(1)

# # Rootdir of the dataset
# #rootdir = "/media/alejandro/DATA/datasets/simdronet"
# ROOTDIR = "/media/alejandro/DATA/datasets/simple_uav_dataset/simulation_fake"
#
# SHOW_IMAGES = 1
# WRITE_ANNOTATIONS = 0
# GENERATE_FOR_COLAB = 0
#
# # Go across all files and subfolders
# for subdir, dirs, files in os.walk(ROOTDIR):
#     for file in files:
#         if file.endswith(".jpg") and not file.startswith("frame_seg"):
#             # Print some info
#             print("Raw filename: " + file)
#             result = re.findall("\d+", file)
#             result = result[0]
#             file_seg = "frame_seg_" + result + ".jpg"
#             print(file_seg)
#
#
#             # Load an color image in grayscale
#             img = cv2.imread(os.path.join(subdir, file))
#             # Load an color image in grayscale
#             img_seg = cv2.imread(os.path.join(subdir, file_seg))
#             mask = cv2.cvtColor(img_seg, cv2.COLOR_BGR2GRAY)
#             ret, mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)
#             mask /= 255
#             # img = cv2.bitwise_and(img, img, mask=mask)
#
#             x, y, w, h = cv2.boundingRect(mask)
#             cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
#
#             if WRITE_ANNOTATIONS:
#                 if not GENERATE_FOR_COLAB:
#                     # Write info in a file
#                     with open(subdir + '/../../annotations.txt', 'a') as the_file:
#                         the_file.write(str(subdir + "/" + file) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
#                                        + str(y + h) + "," + "drone" + '\n')
#                 else:
#                     # Write info in a file
#                     index = subdir.find("simulation/")
#                     temp = subdir[index + 11 :len(subdir)]
#                     with open(subdir + '/../../annotations.txt', 'a') as the_file:
#                         the_file.write(str(temp + "/" + file) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
#                                        + str(y + h) + "," + "drone" + '\n')
#
#             if SHOW_IMAGES:
#                 # Show iamge
#                 cv2.imshow('image', img)
#                 cv2.imshow('image_seg', img_seg)
#                 cv2.waitKey(0)
#
#             #input("Press Enter to continue...")
