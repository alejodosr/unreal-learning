import cv2
import os
import re
import math

# Rootdir of the dataset
#rootdir = "/media/alejandro/DATA/datasets/simdronet"
ROOTDIR = "/media/alejandro/DATA/datasets/simple_m100_dataset_test"

SHOW_IMAGES = 1
WRITE_ANNOTATIONS = 1
GENERATE_FOR_COLAB = 0

FOLDERS_AND_CATS = {"drone": [0], "bird": [1], "car": [2]}

# Go across all files and subfolders
for subdir, dirs, files in os.walk(ROOTDIR):
    for file in files:
        if file.endswith(".jpg") or file.endswith(".png"):
            # Print some info
            print("Raw filename: " + file)
            result = re.findall("\d+", file)
            result = result[0]
            file_seg = "frame_seg_" + result + ".jpg"
            print(file_seg)


            # Load an color image in grayscale
            img = cv2.imread(os.path.join(subdir, file))

            # Select ROI
            r = cv2.selectROI(img)
            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]

            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

            if WRITE_ANNOTATIONS and w != 0 and h != 0:
                anno_number = int(subdir[len(subdir) - 8])

                for key, value in FOLDERS_AND_CATS.items():
                    if anno_number in value:
                        annotation = key
                        break

                if not GENERATE_FOR_COLAB:
                    # Write info in a file
                    with open(subdir + '/../../manual_annotations.txt', 'a') as the_file:
                        the_file.write(str(subdir + "/" + file) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
                                       + str(y + h) + "," + annotation + '\n')
                else:
                    # Write info in a file
                    temp = subdir[len(subdir) - 8 :len(subdir)]
                    with open(subdir + '/../../manual_annotations.txt', 'a') as the_file:
                        the_file.write(str(temp + "/" + file) + "," + str(x) + "," + str(y) + "," + str(x + w) + ","
                                       + str(y + h) + "," + annotation + '\n')

            if SHOW_IMAGES:
                # Show iamge
                cv2.imshow('image', img)
                cv2.waitKey(1)

            #input("Press Enter to continue...")
