from __future__ import division, absolute_import, print_function
import matplotlib

import time; print(time.strftime("The last update of this file: %Y-%m-%d %H:%M:%S", time.gmtime()))

import os, sys, time, re, json
from unrealcv import client
import matplotlib.pyplot as plt
import PIL.Image
import io

import numpy as np, cv2
#from pyHook import HookManager
#from win32gui import PumpMessages, PostQuitMessage
#import threading
#import time
import math

print(" -------------------- NETAIRSIM GENERATE UAV DETECTIONS ---------------------")

# Define parameters
ENABLE_SHOW = True
FREQUENCY = 10.0      # Frames per second
# BASE_FOLDER = "E:/Alejo/datasets/simdronet/steering/"
BASE_FOLDER = "F:\datasets\simple_m100_dataset/"
FOLDER_INIT = 1

# Simple UAV
# QUADROTOR_NAME = "Quadrotor1_2"
# QUADROTOR_COLOR_HSV_LOW = (5,220,220)
# QUADROTOR_COLOR_HSV_HIGH = (23,255,255)

# M100
QUADROTOR_NAME = "M100_Airframe__CAD_2"
QUADROTOR_COLOR_HSV_LOW = (5,220,220)
QUADROTOR_COLOR_HSV_HIGH = (23,255,255)


imread = plt.imread
def imread8(im_file):
    ''' Read image as a 8-bit numpy array '''
    im = np.asarray(Image.open(im_file))
    return im

def read_png(res):
    import StringIO, PIL.Image
    img = PIL.Image.open(StringIO.StringIO(res))
    return np.asarray(img)

def read_npy(res):
    import StringIO
    return np.load(StringIO.StringIO(res))

# Connect to the AirSim simulator
print("NETAIRSIM_GENERATE_UAV_DETECTION: Contacting the UnrealCV client")
client.connect()
if not client.isconnected():
    print('UnrealCV server is not running. Run the game downloaded from http://unrealcv.github.io first.')
    sys.exit(-1)

res = client.request('vget /unrealcv/status')
# The image resolution and port is configured in the config file.
print(res)

# Frame counter
frame_counter = 0
# folder_counter_prev = watcher.folder_counter
folder_counter = FOLDER_INIT

# Create folder
directory = BASE_FOLDER + str(folder_counter)
if not os.path.exists(directory):
    os.makedirs(directory)

directory = BASE_FOLDER + str(folder_counter) + '/images'
if not os.path.exists(directory):
    os.makedirs(directory)

# Init the folder
with open(BASE_FOLDER + str(folder_counter) + '/labels.txt', 'a') as the_file:
    the_file.write("box_ox,box_oy,box_width,box_height,uav_pose_x,uav_pose_y,uav_pose_z,uav_pose_pitch,uav_pose_yaw,uav_pose_roll,cam_pose_x,cam_pose_y,cam_pose_z,cam_pose_pitch,cam_pose_yaw,cam_pose_roll" + '\n')


while True:
    if ENABLE_SHOW:


        # Get camera images from the car
        # res = client.request('vget /camera/0/lit png')
        # res2 = read_png(res)
        # img_rgba = np.array(res2)
        #
        # # Convert to OpenCV standards
        # img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

        # Read segmented image
        # client.request('vrun r.setRes 1000x500w')

        # Get the object mask image
        # print(client.request('vget /objects'))
        # input()

        # Get the object mask image
        res_seg = client.request('vget /camera/0/object_mask png')
        # Get camera images from the car
        res_rgb = client.request('vget /camera/0/lit npy')

        # Convert images
        res2 = np.load(io.BytesIO(res_rgb))
        img_rgba = res2

        # Convert to OpenCV standards
        img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

        res2 = read_png(res_seg)
        img_rgba = np.array(res2)

        # Convert to OpenCV standards
        img_bgra_seg = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

        # Process to get the bounding box
        hsv = cv2.cvtColor(img_bgra_seg, cv2.COLOR_BGR2HSV)

        # Find bounding box
        mask = cv2.inRange(hsv, QUADROTOR_COLOR_HSV_LOW, QUADROTOR_COLOR_HSV_HIGH)
        # img_bgra_seg = cv2.bitwise_and(img_bgra_seg,img_bgra_seg, mask=mask)
        img_bgra_seg = mask

        # Save image
        cv2.imwrite(BASE_FOLDER + str(folder_counter) + '/images/frame_rgb_' + str(frame_counter).zfill(6) + '.jpg',
                    img_bgra[1:721, :])
        cv2.imwrite(BASE_FOLDER + str(folder_counter) + '/images/frame_seg_' + str(frame_counter).zfill(6) + '.jpg',
                    img_bgra_seg[1:721, :])
        frame_counter += 1

        # img_bgra_seg, th1 = cv2.threshold(img_bgra_seg, 127, 255, cv2.THRESH_BINARY)
        x, y, w, h = cv2.boundingRect(mask)
        cv2.rectangle(img_bgra, (x, y), (x + w, y + h), (0, 255, 0), 2)

    else:
        small = np.ones([64, 64, 3])


    # # Get camera images from the car
    # res = client.request('vget /camera/0/object_mask npy')
    # res2 = np.load(io.BytesIO(res))
    # img_rgba = res2
    #
    # # Convert to OpenCV standards
    # img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

    # Compute relative location
    quadrotor_location = client.request('vget /object/' + QUADROTOR_NAME + '/location')
    quadrotor_location = quadrotor_location.split(" ")
    camera_location = client.request('vget /camera/0/location')
    camera_location = camera_location.split(" ")
    relative_location = []
    relative_location.append((float(camera_location[0]) - float(quadrotor_location[0])) / 1E2) # It is in centimeter, so we pass to meters
    relative_location.append((float(camera_location[1]) - float(quadrotor_location[1])) / 1E2)
    relative_location.append((float(camera_location[2]) - float(quadrotor_location[2])) / 1E2)
    # print(relative_location)

    # Compute relative orientation
    quadrotor_rotation = client.request('vget /object/' + QUADROTOR_NAME + '/rotation')
    quadrotor_rotation = quadrotor_rotation.split(" ")
    camera_rotation = client.request('vget /camera/0/rotation') #  Pitch, yaw, roll
    camera_rotation = camera_rotation.split(" ")
    relative_rotation = []
    relative_rotation.append((float(camera_rotation[0]) - float(quadrotor_rotation[0]))) # It is in degrees
    relative_rotation.append((float(camera_rotation[1]) - float(quadrotor_rotation[1])))
    relative_rotation.append((float(camera_rotation[2]) - float(quadrotor_rotation[2])))
    # print(relative_rotation)

    # Save to file
    with open(BASE_FOLDER + str(folder_counter) + '/labels.txt', 'a') as the_file:
         the_file.write(str(x) + "," + str(y) + "," + str(w) + "," + str(h) + "," + str(quadrotor_location[0]) + "," + str(quadrotor_location[1]) + "," + str(quadrotor_location[2]) + "," + str(quadrotor_rotation[0]) + "," + str(quadrotor_rotation[1]) + "," + str(quadrotor_rotation[2])
            + "," + str(camera_location[0]) + "," + str(camera_location[1]) + "," + str(camera_location[2]) + "," + str(camera_rotation[0]) + "," + str(camera_rotation[1]) + "," + str(camera_rotation[2]) + '\n')

    if ENABLE_SHOW:
        # # Text
        # font = cv2.FONT_HERSHEY_SIMPLEX
        # cv2.putText(img_bgra, str(round(math.pi / 2, 2)), (x_extreme_left + 5, y_extreme), font, 0.5, COLOR, 1, cv2.LINE_AA)
        # cv2.putText(img_bgra, str(-round(math.pi / 2, 2)), (x_extreme_right - 53, y_extreme), font, 0.5, COLOR, 1, cv2.LINE_AA)
        #
        # # Include Altitude
        # cv2.putText(img_bgra, "Altitude: " + str(round(camera_altitude, 2)), (x_extreme_left + 65, y_extreme), font, 1.0, COLOR, 2,
        #             cv2.LINE_AA)
        #
        # # Plot text
        # font = cv2.FONT_HERSHEY_SIMPLEX
        # cv2.putText(img_bgra, str(angle), (x_image - 20, y_image - 15), font, 0.5, COLOR, 1, cv2.LINE_AA)
        #
        # # Draw line in image
        # cv2.line(img_bgra, (x_origin, y_origin), (x_image, y_image), COLOR, 2)


        # Plot text
        # font = cv2.FONT_HERSHEY_SIMPLEX
        # cv2.putText(img_bgra, str(camera_altitude), (10, 400), font, 2, (255, 255, 255), 2, cv2.LINE_AA)

        # Resize to preview
        #small = cv2.resize(img_bgra, (0, 0), fx=0.5, fy=0.5)

        # Show image
        cv2.imshow('image', img_bgra)
        # Show image
        cv2.imshow('image_seg', img_bgra_seg)


        # Wait for interaction
        cv2.waitKey(int(math.ceil(1.0/FREQUENCY)))
    else:
        # Show image
        cv2.imshow('image', small)

        # Wait for interaction
        cv2.waitKey(int(math.ceil(1.0 / FREQUENCY)))

# restore to original state
# client.reset()

# Disable control API
# client.enableApiControl(False)



