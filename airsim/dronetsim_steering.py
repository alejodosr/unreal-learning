print(" -------------------- NETAIRSIM GENERATE COLLISION ---------------------")
from AirSimClient import *
import numpy as np, cv2
from pyHook import HookManager
from win32gui import PumpMessages, PostQuitMessage
import threading
import os
import time


import pprint

# Define parameters
ENABLE_SHOW = True
FREQUENCY = 60.0      # Frames per second
BASE_FOLDER = "E:/Alejo/datasets/simdronet/steering/"
FOLDER_INIT = 14

RO_SIZE = 100
OFFSET_X = 70
OFFSET_Y = 430
COLOR = (100, 100, 222)
BUFFER_SIZE = 3
MULTIPLIER = 3
SCALER = 3
EPSILON = 0.000001

# class Keystroke_Watcher(object):
#     def __init__(self):
#         self.hm = HookManager()
#         self.hm.KeyDown = self.on_keyboard_event
#         self.hm.HookKeyboard()
#         self.keyboard_id = 0
#         self.record_enabled = False
#         self.folder_counter = FOLDER_INIT


#     def on_keyboard_event(self, event):
#         try:
#             # print(str(event.KeyID))
#             if event.KeyID == 90:  # 'z' stands for record
#                 print("NETAIRSIM_GENERATE_STEERING: START recording")
#
#                 self.folder_counter = self.folder_counter + 1
#                 directory = BASE_FOLDER + str(self.folder_counter)
#                 if not os.path.exists(directory):
#                     os.makedirs(directory)
#
#                 directory = BASE_FOLDER + str(self.folder_counter) + '/images'
#                 if not os.path.exists(directory):
#                     os.makedirs(directory)
#
#                 # Start recording
#                 self.record_enabled = True
#
#             if event.KeyID == 88:  # 'x' stands for stop recording
#                 print("NETAIRSIM_GENERATE_STEERING: STOP recording")
#                 # Stop recording
#                 self.record_enabled = False
#
#                 # Remove last line
#                 with open(BASE_FOLDER + str(watcher.folder_counter) + '/labels.txt', 'rb+') as the_file:
#                     the_file.seek(-1, os.SEEK_END)
#                     the_file.truncate()
#                     the_file.seek(-1, os.SEEK_END)
#                     the_file.truncate()
#                     # the_file.seek(-1, os.SEEK_END)
#                     # the_file.truncate()
#                     # the_file.seek(-1, os.SEEK_END)
#                     # the_file.truncate()
#                     # the_file.seek(-1, os.SEEK_END)
#                     # the_file.truncate()
#
#             if event.KeyID == 160:  # Left Shift
#                 print("NETAIRSIM_GENERATE_STEERING: Change label pressed")
#                 self.keyboard_id = ~self.keyboard_id
#             # Abort execution
#             if event.KeyID == 27:
#                 print("NETAIRSIM_GENERATE_STEERING: Exit performed by the user. Success.")
#                 os._exit(1)
#         finally:
#             return True
#
#     def your_method(self):
#         pass
#
#     def shutdown(self):
#         PostQuitMessage(0)
#         self.hm.UnhookKeyboard()
#
# def receive_from_keyboard_thread():
#     PumpMessages()

# Connect to the AirSim simulator
print("NETAIRSIM_GENERATE_STEERING: Contacting the Airsim car client")
client = CarClient()
client.confirmConnection()
#client.enableApiControl(False)
#car_controls = CarControls()

# Start receiving from keyboard
print("NETAIRSIM_GENERATE_STEERING: Generating keyboard events watcher")
# threads = list()
# t = threading.Thread(target=receive_from_keyboard_thread)
# threads.append(t)
# t.start()

# Instantiate class
# watcher = Keystroke_Watcher()

# Frame counter
frame_counter = 0
folder_counter = FOLDER_INIT

pp = pprint.PrettyPrinter(indent=4)

camera_locations = []
angle_prev = 0

# Create folder
directory = BASE_FOLDER + str(folder_counter)
if not os.path.exists(directory):
    os.makedirs(directory)

directory = BASE_FOLDER + str(folder_counter) + '/images'
if not os.path.exists(directory):
    os.makedirs(directory)

# Init the folder
with open(BASE_FOLDER + str(folder_counter) + '/sync_steering.txt', 'a') as the_file:
    the_file.write("angle, altitude" + '\n')

while True:
    # Read actual time
    #now = time.time()

    # Get camera images from the car
    responses = client.simGetImages([
        ImageRequest(0, AirSimImageType.Scene, False, False)])  # scene vision image in png format

    # Simple parse of the response
    response = responses[0]

    # Get numpy array
    img = np.fromstring(response.image_data_uint8, dtype=np.uint8)

    # reshape array to 4 channel image array H X W X 4
    img_rgba = img.reshape(response.height, response.width, 4)

    # original image is fliped vertically
    #img_rgba = np.flipud(img_rgba)

    # Convert to OpenCV standards
    img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

    # Get camera info
    camera_info = client.getCameraInfo(0)
    #
    # camera_location = [camera_info.pose.position.x_val, camera_info.pose.position.y_val, -camera_info.pose.position.z_val]
    # print(str(camera_location))

    car_state = client.getCarState()
    camera_location = [car_state.kinematics_true.position.x_val, car_state.kinematics_true.position.y_val, -camera_info.pose.position.z_val]
    # print(car_state)
    # euler_car = client.toEulerianAngle(car_state.kinematics_true.orientation)

    camera_altitude = camera_location[2]

    if len(camera_locations) < BUFFER_SIZE * MULTIPLIER:
        # Append
        camera_locations.append(camera_location)
    else:
        camera_locations = camera_locations[1:] + [camera_locations[0]]
        camera_locations[BUFFER_SIZE * MULTIPLIER - 1] = camera_location

    # If the buffer is ready
    if len(camera_locations) == BUFFER_SIZE * MULTIPLIER:

        # Definitions
        x0 = float(camera_locations[0 * MULTIPLIER][0])
        y0 = float(camera_locations[0 * MULTIPLIER][1])
        x1 = float(camera_locations[1 * MULTIPLIER][0])
        y1 = float(camera_locations[1 * MULTIPLIER][1])
        x2 = float(camera_locations[2 * MULTIPLIER][0])
        y2 = float(camera_locations[2 * MULTIPLIER][1])

        # Compute derivatives
        m_ant = (x0 - x1) / (y0 - y1 + EPSILON)
        m_cur = (x1 - x2) / (y1 - y2 + EPSILON)
        angle = (-1) * math.atan2((m_cur - m_ant), (1 + m_ant * m_cur)) * SCALER

        if angle > math.pi / 2:
            angle -= (2 * math.pi)

        if angle < - math.pi / 2:
            angle += (2 * math.pi)

        if(abs(angle) > 1.80):
            angle = prev_angle

        prev_angle = angle

        # In order to adjust it to CityScapes Dataset
        angle = - angle


        # Save image
        cv2.imwrite(BASE_FOLDER + str(folder_counter) + '/images/frame_' + str(frame_counter).zfill(6) + '.jpg', img_bgra[1:721, :])

        # Save to file
        with open(BASE_FOLDER + str(folder_counter) + '/sync_steering.txt', 'a') as the_file:
             the_file.write(str(angle) + "," + str(camera_altitude) + '\n')

        frame_counter = frame_counter + 1

        if ENABLE_SHOW:
            # Adapt it to plot it
            angle = - angle

            # From polars to cartesians
            x_image = int(RO_SIZE * math.sin(angle) + RO_SIZE + OFFSET_X)
            y_image = int(- (RO_SIZE * math.cos(angle)) + OFFSET_Y)

            x_origin = OFFSET_X + RO_SIZE
            y_origin = OFFSET_Y

            # Plot extremes
            # Line
            x_extreme_left = int(RO_SIZE * math.sin(math.pi / 2) + RO_SIZE + OFFSET_X)
            y_extreme = int(- (RO_SIZE * math.cos(math.pi / 2)) + OFFSET_Y)
            x_extreme_right = int(RO_SIZE * math.sin(-math.pi / 2) + RO_SIZE + OFFSET_X)
            cv2.line(img_bgra, (x_extreme_left, y_extreme), (x_extreme_right, y_extreme), COLOR, 2)

            # Text
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img_bgra, str(round(math.pi / 2, 2)), (x_extreme_left + 5, y_extreme), font, 0.5, COLOR, 1,
                        cv2.LINE_AA)
            cv2.putText(img_bgra, str(-round(math.pi / 2, 2)), (x_extreme_right - 53, y_extreme), font, 0.5, COLOR, 1,
                        cv2.LINE_AA)

            # Include Altitude
            cv2.putText(img_bgra, "Altitude: " + str(round(camera_altitude, 2)), (x_extreme_left + 65, y_extreme), font,
                        1.0, COLOR, 2,
                        cv2.LINE_AA)

            # Plot text
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img_bgra, str(angle), (x_image - 20, y_image - 15), font, 0.5, COLOR, 1, cv2.LINE_AA)

            # Draw line in image
            cv2.line(img_bgra, (x_origin, y_origin), (x_image, y_image), COLOR, 2)

            # Resize to preview
            # small = cv2.resize(img_bgra, (0, 0), fx=0.5, fy=0.5)

            # Show image
            cv2.imshow('image', img_bgra)

            # Wait for interaction
            cv2.waitKey(1)
        else:
            # Sleep for the precise time
            time.sleep(10)


        # Read the elapsed time
        #elapsed = time.time() - now



# restore to original state
# client.reset()

# Disable control API
# client.enableApiControl(False)



