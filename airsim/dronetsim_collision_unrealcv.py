from __future__ import division, absolute_import, print_function
import matplotlib

import time; print(time.strftime("The last update of this file: %Y-%m-%d %H:%M:%S", time.gmtime()))

import os, sys, time, re, json
import matplotlib.pyplot as plt
from unrealcv import client
import PIL.Image
import io

import numpy as np, cv2
from pyHook import HookManager
from win32gui import PumpMessages, PostQuitMessage
import threading
import time
import math

print(" -------------------- NETAIRSIM GENERATE COLLISION ---------------------")

# Define parameters
ENABLE_SHOW = True
FREQUENCY = 30.0      # Frames per second
BASE_FOLDER = "C:/Users/Admin/Documents/dataset/simdronet/collision/"
FOLDER_INIT = 133

class Keystroke_Watcher(object):
    def __init__(self):
        self.hm = HookManager()
        self.hm.KeyDown = self.on_keyboard_event
        self.hm.HookKeyboard()
        self.keyboard_id = 0
        self.record_enabled = False
        self.folder_counter = FOLDER_INIT


    def on_keyboard_event(self, event):
        try:
            #print(str(event.KeyID))
            if event.KeyID == 90:   # 'z' stands for record
                print("NETAIRSIM_GENERATE_COLLISION: START recording")

                self.folder_counter = self.folder_counter + 1
                directory = BASE_FOLDER + str(self.folder_counter)
                if not os.path.exists(directory):
                    os.makedirs(directory)

                directory = BASE_FOLDER + str(self.folder_counter) + '/images'
                if not os.path.exists(directory):
                    os.makedirs(directory)

                # Start recording
                self.record_enabled = True

            if event.KeyID == 88:  # 'x' stands for stop recording
                print("NETAIRSIM_GENERATE_COLLISION: STOP recording")
                # Stop recording
                self.record_enabled = False

                # Remove last line
                with open(BASE_FOLDER + str(watcher.folder_counter) + '/labels.txt', 'rb+') as the_file:
                    the_file.seek(-1, os.SEEK_END)
                    the_file.truncate()
                    the_file.seek(-1, os.SEEK_END)
                    the_file.truncate()
                    # the_file.seek(-1, os.SEEK_END)
                    # the_file.truncate()
                    # the_file.seek(-1, os.SEEK_END)
                    # the_file.truncate()
                    # the_file.seek(-1, os.SEEK_END)
                    # the_file.truncate()

            if event.KeyID == 160:   # Left Shift
                print("NETAIRSIM_GENERATE_COLLISION: Change label pressed")
                self.keyboard_id = ~self.keyboard_id
            # Abort execution
            if event.KeyID == 27:
                print("NETAIRSIM_GENERATE_COLLISION: Exit performed by the user. Success.")
                os._exit(1)
        finally:
            return True

    def your_method(self):
        pass

    def shutdown(self):
        PostQuitMessage(0)
        self.hm.UnhookKeyboard()

def receive_from_keyboard_thread():
    PumpMessages()


imread = plt.imread
def imread8(im_file):
    ''' Read image as a 8-bit numpy array '''
    im = np.asarray(Image.open(im_file))
    return im

def read_png(res):
    img = PIL.Image.open(io.BytesIO(res))
    return np.asarray(img)

def read_npy(res):
    return np.load(io.BytesIO(res))

# It is also possible to get the png directly without saving to a file
# res = client.request('vget /camera/0/lit C:/Users/Admin/Desktop/lit.png')

# Connect to the AirSim simulator
print("NETAIRSIM_GENERATE_COLLISION: Contacting the UnrealCV client")
client.connect()
if not client.isconnected():
    print('UnrealCV server is not running. Run the game downloaded from http://unrealcv.github.io first.')
    sys.exit(-1)

res = client.request('vget /unrealcv/status')
# The image resolution and port is configured in the config file.
print(res)

# Start receiving from keyboard
print("NETAIRSIM_GENERATE_COLLISION: Generating keyboard events watcher")
threads = list()
t = threading.Thread(target=receive_from_keyboard_thread)
threads.append(t)
t.start()

# Instantiate class
watcher = Keystroke_Watcher()

# Frame counter
frame_counter = 0
folder_counter_prev = watcher.folder_counter

while True:
    if ENABLE_SHOW:
        # Get camera images from the car
        res = client.request('vget /camera/0/lit npy')
        res2 = np.load(io.BytesIO(res))
        img_rgba = res2

        # Convert to OpenCV standards
        img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

        # Simple parse of the response

        #response = responses[0]

        # Get numpy array
        #img = np.fromstring(res2.image_data_uint8, dtype=np.uint8)

        # reshape array to 4 channel image array H X W X 4
        #img_rgba = img.reshape(img.height, img.width, 4)

        # original image is fliped vertically
        #img_rgba = np.flipud(img_rgba)

    else:
        small = np.ones([64, 64, 3])

    if watcher.record_enabled:
        # Get camera images from the car
        res = client.request('vget /camera/0/lit npy')
        res2 = np.load(io.BytesIO(res))
        img_rgba = res2

        # Convert to OpenCV standards
        img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

        # Save image
        cv2.imwrite(BASE_FOLDER + str(watcher.folder_counter) + '/images/frame_' + str(frame_counter) + '.jpg', img_bgra[1:721, :])
        #res = client.request('vget /camera/0/lit ' + BASE_FOLDER + str(watcher.folder_counter) + '/images/frame_' + str(frame_counter) + '.png')

        # Save to file
        with open(BASE_FOLDER + str(watcher.folder_counter) + '/labels.txt', 'a') as the_file:
            the_file.write(str(abs(watcher.keyboard_id))+'\n')

        # Check for reset of the counter
        if folder_counter_prev != watcher.folder_counter:
            folder_counter_prev = watcher.folder_counter
            frame_counter = 0
        else:
            frame_counter = frame_counter + 1

    if ENABLE_SHOW:
        # Plot text
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img_bgra, str(abs(watcher.keyboard_id)), (10, 500), font, 4, (255, 255, 255), 2, cv2.LINE_AA)

        # Resize to preview
        small = cv2.resize(img_bgra, (0, 0), fx=0.5, fy=0.5)

        # Show image
        cv2.imshow('image', small)

        # Wait for interaction
        cv2.waitKey(int(math.ceil(1.0/FREQUENCY)))
    else:
        # Show image
        cv2.imshow('image', small)

        # Wait for interaction
        cv2.waitKey(int(math.ceil(1.0 / FREQUENCY)))

# restore to original state
# client.reset()

# Disable control API
# client.enableApiControl(False)



