from __future__ import division, absolute_import, print_function
import matplotlib

import time; print(time.strftime("The last update of this file: %Y-%m-%d %H:%M:%S", time.gmtime()))

import os, sys, time, re, json
import matplotlib.pyplot as plt
from unrealcv import client
import PIL.Image
import io

import numpy as np, cv2
from pyHook import HookManager
from win32gui import PumpMessages, PostQuitMessage
import threading
import time
import math

print(" -------------------- NETAIRSIM GENERATE STEERING ---------------------")

# Define parameters
ENABLE_SHOW = True
FREQUENCY = 60.0      # Frames per second
BASE_FOLDER = "E:/Alejo/datasets/simdronet/steering/"
FOLDER_INIT = 0

RO_SIZE = 100
OFFSET_X = 70
OFFSET_Y = 430
COLOR = (100, 100, 222)
BUFFER_SIZE = 3
MULTIPLIER = 3
SCALER = 2
EPSILON = 0.000001

# class Keystroke_Watcher(object):
#     def __init__(self):
#         self.hm = HookManager()
#         self.hm.KeyDown = self.on_keyboard_event
#         self.hm.HookKeyboard()
#         self.keyboard_id = 0
#         self.record_enabled = False
#         self.folder_counter = FOLDER_INIT
#
#
#     def on_keyboard_event(self, event):
#         try:
#             #print(str(event.KeyID))
#             if event.KeyID == 90:   # 'z' stands for record
#                 print("NETAIRSIM_GENERATE_STEERING: START recording")
#
#                 self.folder_counter = self.folder_counter + 1
#                 directory = BASE_FOLDER + str(self.folder_counter)
#                 if not os.path.exists(directory):
#                     os.makedirs(directory)
#
#                 directory = BASE_FOLDER + str(self.folder_counter) + '/images'
#                 if not os.path.exists(directory):
#                     os.makedirs(directory)
#
#                 # Start recording
#                 self.record_enabled = True
#
#             if event.KeyID == 88:  # 'x' stands for stop recording
#                 print("NETAIRSIM_GENERATE_STEERING: STOP recording")
#                 # Stop recording
#                 self.record_enabled = False
#
#                 # Remove last line
#                 with open(BASE_FOLDER + str(watcher.folder_counter) + '/labels.txt', 'rb+') as the_file:
#                     the_file.seek(-1, os.SEEK_END)
#                     the_file.truncate()
#                     the_file.seek(-1, os.SEEK_END)
#                     the_file.truncate()
#                     # the_file.seek(-1, os.SEEK_END)
#                     # the_file.truncate()
#                     # the_file.seek(-1, os.SEEK_END)
#                     # the_file.truncate()
#                     # the_file.seek(-1, os.SEEK_END)
#                     # the_file.truncate()
#
#             if event.KeyID == 160:   # Left Shift
#                 print("NETAIRSIM_GENERATE_STEERING: Change label pressed")
#                 self.keyboard_id = ~self.keyboard_id
#             # Abort execution
#             if event.KeyID == 27:
#                 print("NETAIRSIM_GENERATE_STEERING: Exit performed by the user. Success.")
#                 os._exit(1)
#         finally:
#             return True
#
#     def your_method(self):
#         pass
#
#     def shutdown(self):
#         PostQuitMessage(0)
#         self.hm.UnhookKeyboard()
#
# def receive_from_keyboard_thread():
#     PumpMessages()


imread = plt.imread
def imread8(im_file):
    ''' Read image as a 8-bit numpy array '''
    im = np.asarray(Image.open(im_file))
    return im

def read_png(res):
    img = PIL.Image.open(io.BytesIO(res))
    return np.asarray(img)

def read_npy(res):
    return np.load(io.BytesIO(res))

# It is also possible to get the png directly without saving to a file
# res = client.request('vget /camera/0/lit C:/Users/Admin/Desktop/lit.png')

# Connect to the AirSim simulator
print("NETAIRSIM_GENERATE_STEERING: Contacting the UnrealCV client")
client.connect()
if not client.isconnected():
    print('UnrealCV server is not running. Run the game downloaded from http://unrealcv.github.io first.')
    sys.exit(-1)

res = client.request('vget /unrealcv/status')
# The image resolution and port is configured in the config file.
print(res)

# Start receiving from keyboard
# print("NETAIRSIM_GENERATE_STEERING: Generating keyboard events watcher")
# threads = list()
# t = threading.Thread(target=receive_from_keyboard_thread)
# threads.append(t)
# t.start()
#
# # Instantiate class
# watcher = Keystroke_Watcher()

# Frame counter
frame_counter = 0
# folder_counter_prev = watcher.folder_counter
folder_counter = FOLDER_INIT

# Create folder
directory = BASE_FOLDER + str(folder_counter)
if not os.path.exists(directory):
    os.makedirs(directory)

directory = BASE_FOLDER + str(folder_counter) + '/images'
if not os.path.exists(directory):
    os.makedirs(directory)

# Init the folder
with open(BASE_FOLDER + str(folder_counter) + '/sync_steering.txt', 'a') as the_file:
    the_file.write("angle, altitude" + '\n')

# Init the camera yaw
init_altitude = 100 # cm
camera_locations = []
angle_prev = 0


while True:
    if ENABLE_SHOW:
        # Get camera images from the car
        res = client.request('vget /camera/0/lit npy')
        res2 = np.load(io.BytesIO(res))
        img_rgba = res2

        # Convert to OpenCV standards
        img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

        # Simple parse of the response

        #response = responses[0]

        # Get numpy array
        #img = np.fromstring(res2.image_data_uint8, dtype=np.uint8)

        # reshape array to 4 channel image array H X W X 4
        #img_rgba = img.reshape(img.height, img.width, 4)

        # original image is fliped vertically
        #img_rgba = np.flipud(img_rgba)

    else:
        small = np.ones([64, 64, 3])


    # Get camera images from the car
    res = client.request('vget /camera/0/lit npy')
    res2 = np.load(io.BytesIO(res))
    img_rgba = res2

    # Convert to OpenCV standards
    img_bgra = cv2.cvtColor(img_rgba, cv2.COLOR_RGBA2BGRA)

    #res = client.request('vget /camera/0/lit ' + BASE_FOLDER + str(watcher.folder_counter) + '/images/frame_' + str(frame_counter) + '.png')

    # camera_rotation = client.request('vget /camera/0/rotation')
    # camera_rotation = camera_rotation.split(" ")
    # camera_yaw = ((float(camera_rotation[1]) - init_yaw) * math.pi / 180)
    # print("Camera yaw angle: " + str(camera_yaw))

    camera_location = client.request('vget /camera/0/location')
    # print ("Camera location: " + camera_location)
    # print("Camera vector size: " + str(len(camera_locations)))
    # print("Camera lcoation vector: " + str(camera_locations))
    camera_location = camera_location.split(" ")
    camera_altitude = (float(camera_location[2]) - init_altitude) / 100
    print("Camera altitude: " + str(camera_altitude))

    if len(camera_locations) < BUFFER_SIZE * MULTIPLIER:
        # Append
        camera_locations.append(camera_location)
    else:
        camera_locations = camera_locations[1:] + [camera_locations[0]]
        camera_locations[BUFFER_SIZE * MULTIPLIER - 1] = camera_location

    # If the buffer is ready
    if len(camera_locations) == BUFFER_SIZE * MULTIPLIER:
        # Save image
        cv2.imwrite(BASE_FOLDER + str(folder_counter) + '/images/frame_' + str(frame_counter).zfill(6) + '.jpg',
                    img_bgra[1:721, :])
        frame_counter += 1

        # Definitions
        x0 = float(camera_locations[0 * MULTIPLIER][0])
        y0 = float(camera_locations[0 * MULTIPLIER][1])
        x1 = float(camera_locations[1 * MULTIPLIER][0])
        y1 = float(camera_locations[1 * MULTIPLIER][1])
        x2 = float(camera_locations[2 * MULTIPLIER][0])
        y2 = float(camera_locations[2 * MULTIPLIER][1])

        # Compute derivatives
        m_ant = (x0 - x1) / (y0 - y1 + EPSILON)
        m_cur = (x1 - x2) / (y1 - y2 + EPSILON)
        angle = (-1) * math.atan2((m_cur - m_ant), (1 + m_ant * m_cur)) * SCALER

        if angle > math.pi / 2:
            angle -= (2 * math.pi)

        if angle < - math.pi / 2:
            angle += (2 * math.pi)

        prev_angle = angle

        # In order to adjust it to CityScapes Dataset
        angle = - angle

        # Save to file
        with open(BASE_FOLDER + str(folder_counter) + '/sync_steering.txt', 'a') as the_file:
             the_file.write(str(angle) + "," + str(camera_altitude) + '\n')

        # Check for reset of the counter
        # if folder_counter_prev != watcher.folder_counter:
        #     folder_counter_prev = watcher.folder_counter
        #     frame_counter = 0w
        # else:
        #     frame_counter = frame_counter + 1

        if ENABLE_SHOW:
            # Adapt it to plot it
            angle = - angle

            # From polars to cartesians
            x_image = int(RO_SIZE * math.sin(angle) + RO_SIZE + OFFSET_X)
            y_image = int(- (RO_SIZE * math.cos(angle)) + OFFSET_Y)

            x_origin = OFFSET_X + RO_SIZE
            y_origin = OFFSET_Y

            # Plot extremes
            # Line
            x_extreme_left = int(RO_SIZE * math.sin(math.pi / 2) + RO_SIZE + OFFSET_X)
            y_extreme = int(- (RO_SIZE * math.cos(math.pi / 2)) + OFFSET_Y)
            x_extreme_right = int(RO_SIZE * math.sin(-math.pi / 2) + RO_SIZE + OFFSET_X)
            cv2.line(img_bgra, (x_extreme_left, y_extreme), (x_extreme_right, y_extreme), COLOR, 2)

            # Text
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img_bgra, str(round(math.pi / 2, 2)), (x_extreme_left + 5, y_extreme), font, 0.5, COLOR, 1, cv2.LINE_AA)
            cv2.putText(img_bgra, str(-round(math.pi / 2, 2)), (x_extreme_right - 53, y_extreme), font, 0.5, COLOR, 1, cv2.LINE_AA)

            # Include Altitude
            cv2.putText(img_bgra, "Altitude: " + str(round(camera_altitude, 2)), (x_extreme_left + 65, y_extreme), font, 1.0, COLOR, 2,
                        cv2.LINE_AA)

            # Plot text
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img_bgra, str(angle), (x_image - 20, y_image - 15), font, 0.5, COLOR, 1, cv2.LINE_AA)

            # Draw line in image
            cv2.line(img_bgra, (x_origin, y_origin), (x_image, y_image), COLOR, 2)


            # Plot text
            # font = cv2.FONT_HERSHEY_SIMPLEX
            # cv2.putText(img_bgra, str(camera_altitude), (10, 400), font, 2, (255, 255, 255), 2, cv2.LINE_AA)

            # Resize to preview
            #small = cv2.resize(img_bgra, (0, 0), fx=0.5, fy=0.5)

            # Show image
            cv2.imshow('image', img_bgra)

            # Wait for interaction
            cv2.waitKey(int(math.ceil(1.0/FREQUENCY)))
        else:
            # Show image
            cv2.imshow('image', small)

            # Wait for interaction
            cv2.waitKey(int(math.ceil(1.0 / FREQUENCY)))

# restore to original state
# client.reset()

# Disable control API
# client.enableApiControl(False)



