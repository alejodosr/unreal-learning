import numpy as np
import cv2
import os

# Rootdir of the dataset
ROOTVIDEO = "/home/alejandro/workspace/OpenTracker/sequences/landing.mp4"
BASE_NAME = "frame"
OUTDIR = "/home/alejandro/workspace/OpenTracker/sequences/Landing/img"
FINAL_RESOLUTION_WIDTH = 960
FINAL_RESOLUTION_HEIGHT = 720
ENABLE_RESIZE = 0
FRAMES_SKIP = 1
CROP_VALUE_X = 0
CROP_VALUE_Y = 500
ZFILL_AMOUNT = 4

# Read from video
cap = cv2.VideoCapture(ROOTVIDEO)

# Check if camera opened successfully
if (cap.isOpened()== False):
  print("Error opening video stream or file")

# Counter
counter = 0

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    if ENABLE_RESIZE:
        # Resize frame NOTE: BUG IN SHAPE VALUE
        frame = cv2.resize(frame[CROP_VALUE_X:frame.shape[0] - CROP_VALUE_X, CROP_VALUE_Y:frame.shape[1] - CROP_VALUE_Y], (FINAL_RESOLUTION_WIDTH, FINAL_RESOLUTION_HEIGHT))

    # Increase counter
    counter = counter + 1

    # Save file
    if counter % FRAMES_SKIP == 0:
        # cv2.imwrite(OUTDIR + "/" + BASE_NAME + "_" + str(int(counter / FRAMES_SKIP)).zfill(ZFILL_AMOUNT) + ".jpg", frame)
        cv2.imwrite(OUTDIR + "/" + str(int(counter / FRAMES_SKIP)).zfill(ZFILL_AMOUNT) + ".jpg", frame)

    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

# Print final number of wrong images
print("FINAL NUMBER OF IMAGES: " + str(counter))
