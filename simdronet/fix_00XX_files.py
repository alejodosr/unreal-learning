import os
import re

# Rootdir of the dataset
#rootdir = "/media/alejandro/DATA/datasets/simdronet"
rootdir = "/media/alejandro/DATA/datasets/simdronet/final_test/collision/1/images"

	
# Go across all files and subfolders
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        print(os.path.join(subdir, file))
        if file.endswith(".jpg"):
            print("Raw filename: " + file)
            result = re.findall("\d+", file)
            result = result[0]
            #print(result)

            # Fill with zeros
            result = result.zfill(6)
            #print(result)

            # Create new name
            final_name = "frame_" + result + ".jpg"
            print("Final filename: " + final_name)

            # Rename file
            os.rename(os.path.join(subdir, file), os.path.join(subdir, final_name))

            #input("Press Enter to continue...")
