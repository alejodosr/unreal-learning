import numpy as np
import cv2
import os

# Rootdir of the dataset
rootdir = "/media/alejandro/DATA/datasets/simdronet"
counter = 0

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        print(os.path.join(subdir, file))
        if file.endswith(".jpg"):
            # Load an color image in grayscale
            img = cv2.imread(os.path.join(subdir, file))

            # Check whether it has wrong number of rows
            if img.shape[0] != 720:
                counter += 1
                print("Wrong number of rows (" + str(counter) + ")")

                #resized_image = cv2.resize(img, (960, 720))

                # Save image
                #cv2.imwrite(os.path.join(subdir, file), resized_image)

                # Show image
                #cv2.imshow('image', img)
                #cv2.imshow('image_resized', resized_image)
                #cv2.waitKey(0)

# Print final number of wrong images
print("FINAL NUMBER OF WRONG IMAGES: " + str(counter))


