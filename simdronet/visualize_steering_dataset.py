import cv2
import os
import re
import math

# Rootdir of the dataset
#rootdir = "/media/alejandro/DATA/datasets/simdronet"
ROOTDIR = "E:/Alejo/datasets/simdronet/steering/13"
RO_SIZE = 100
OFFSET_X = 70
OFFSET_Y = 430
COLOR = (100, 100, 222)

# Go across all files and subfolders
for subdir, dirs, files in os.walk(ROOTDIR):
    for file in files:
        if file.endswith(".txt"):
            # Steering angles file
            print(os.path.join(subdir, file))

            # Open to parse the labels
            with open(os.path.join(subdir, file)) as fp:
                # Read all the lines
                content = fp.readlines()

                # you may also want to remove whitespace characters like `\n` at the end of each line
                content = [x.strip() for x in content]

                # Reset image counter
                image_counter = 1

        if file.endswith(".jpg"):
            # Print some info
            print("Raw filename: " + file)

            # Load an color image in grayscale
            img = cv2.imread(os.path.join(subdir, file))

            # Read current angle
            current_line = content[image_counter].split(",")
            current_angle = round((-1) * float(current_line[0]), 2)

            #  Print current angle value
            print("Current angle: " + str(current_angle))

            # Increase counter
            image_counter += 1

            # Print aspect ratio of images
            # height, width, channels = img.shape
            # print("Image  Width: " + str(width))
            # print("Image  Height: " + str(height))

            # From polars to cartesians
            x_image = int(RO_SIZE * math.sin(current_angle) + RO_SIZE + OFFSET_X)
            y_image = int(- (RO_SIZE * math.cos(current_angle)) + OFFSET_Y)

            x_origin = OFFSET_X + RO_SIZE
            y_origin = OFFSET_Y

            # Plot extremes
            # Line
            x_extreme_left = int(RO_SIZE * math.sin(math.pi / 2) + RO_SIZE + OFFSET_X)
            y_extreme = int(- (RO_SIZE * math.cos(math.pi / 2)) + OFFSET_Y)
            x_extreme_right = int(RO_SIZE * math.sin(-math.pi / 2) + RO_SIZE + OFFSET_X)
            cv2.line(img, (x_extreme_left, y_extreme), (x_extreme_right, y_extreme), COLOR, 2)

            # Text
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img, str(round(math.pi / 2, 2)), (x_extreme_left + 5, y_extreme), font, 0.5, COLOR, 1, cv2.LINE_AA)
            cv2.putText(img, str(-round(math.pi / 2, 2)), (x_extreme_right - 53, y_extreme), font, 0.5, COLOR, 1, cv2.LINE_AA)


            # Plot text
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(img, str(current_angle), (x_image - 20, y_image - 15), font, 0.5, COLOR, 1, cv2.LINE_AA)

            # Draw line in image
            cv2.line(img, (x_origin, y_origin), (x_image, y_image), COLOR, 2)

            # Show iamge
            cv2.imshow('image', img)
            cv2.waitKey(30)

            #input("Press Enter to continue...")
