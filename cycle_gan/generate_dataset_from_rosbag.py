import rospy
import cv2
# from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import tf
import os

class ImageFromRosbag(object):
    def __init__(self):

        self.IMAGE_TOPIC = "/SQ01/camera/color/image_raw"
        self.FOLDER_INIT = 0
        self.BASE_FOLDER = "/media/alejandro/DATA/datasets/simple_uav_dataset/real/"
        self.SHOW_IMAGE = False

        # Read tf
        self.listener = tf.TransformListener()

        self.bridge = CvBridge()

        # Frame counter
        self.frame_counter = 0
        # folder_counter_prev = watcher.folder_counter
        self.folder_counter = self.FOLDER_INIT

        # Create folder
        self.directory = self.BASE_FOLDER + str(self.folder_counter)
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

            self.directory = self.BASE_FOLDER + str(self.folder_counter) + '/images'
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        # Init the folder
        with open(self.BASE_FOLDER + str(self.folder_counter) + '/labels.txt', 'a') as the_file:
            the_file.write("box_ox,box_oy,box_width,box_height,pose_x,pose_y,pose_z,pose_pitch,pose_yaw,pose_roll" + '\n')

        # Call the subscriber
        self.image_subcriber()


    def image_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        try:
            # Read tf
            # (trans_sq01,rot_sq01) = listener.lookupTransform('/world', '/SQ02/trans', rospy.Time(0))
            # print("Camera position (world): " + str(trans_sq01))
            # (trans_parrot, rot_parrot) = listener.lookupTransform('/world', '/parrot/trans', rospy.Time(0))
            # print("Parrot position (world): " +  str(trans_parrot))
            (trans_sq01,rot_sq01) = self.listener.lookupTransform('/parrot/trans', '/SQ02/trans', rospy.Time(0))

            #  Generate relative position
            # relative_location = []
            # relative_location.append((float(trans_sq01[0]) - float(trans_parrot[0])))  # It is meters
            # relative_location.append((float(trans_sq01[1]) - float(trans_parrot[1])))
            # relative_location.append((float(trans_sq01[2]) - float(trans_parrot[2])))

            relative_location = []
            relative_location.append(float(trans_sq01[0]))  # It is meters
            relative_location.append(float(trans_sq01[1]))
            relative_location.append(float(trans_sq01[2]))

            relative_rotation = []
            relative_rotation.append(float(rot_sq01[0]))  # It is meters
            relative_rotation.append(float(rot_sq01[1]))
            relative_rotation.append(float(rot_sq01[2]))

            print(relative_location)

            # Save to file
            with open(self.BASE_FOLDER + str(self.folder_counter) + '/labels.txt', 'a') as the_file:
                the_file.write(
                    str(0) + "," + str(0) + "," + str(0) + "," + str(0) + "," + str(relative_location[0]) + "," + str(
                        relative_location[1]) + "," + str(relative_location[2]) + "," + str(
                        relative_rotation[0]) + "," + str(relative_rotation[1]) + "," + str(relative_rotation[2]) + '\n')

            # Flip image horizontally
            cv_image = cv2.flip(cv_image, 0)

            # Save image
            cv2.imwrite(self.BASE_FOLDER + str(self.folder_counter) + '/images/frame_rgb_' + str(self.frame_counter).zfill(6) + '.jpg',
                        cv_image)

            self.frame_counter += 1

            if self.SHOW_IMAGE:
                cv2.imshow("Image window", cv_image)
                cv2.waitKey(3)

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

    def image_subcriber(self):
        # In ROS, nodes are uniquely named. If two nodes with the same
        # node are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        rospy.init_node('image_subcriber', anonymous=True)

        rospy.Subscriber(self.IMAGE_TOPIC, Image, self.image_callback)

        rospy.spin()

if __name__ == '__main__':
    ImageFromRosbag()