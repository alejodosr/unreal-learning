import rospy
import cv2
# from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import tf
import os
from cv2 import VideoWriter, VideoWriter_fourcc, imread, resize
import time

class ImageFromRosbag(object):
    def __init__(self):

        self.IMAGE_TOPIC = "/SQ01/camera/color/image_raw"
        self.FOLDER_INIT = 0
        self.BASE_FOLDER = "/media/alejandro/DATA/datasets/simple_uav_dataset/real/"
        self.SHOW_IMAGE = False
        self.FPS = 30
        self.MAX_TIME = 10

        # Read tf
        self.listener = tf.TransformListener()

        self.bridge = CvBridge()

        # Frame counter
        self.frame_counter = 0
        # folder_counter_prev = watcher.folder_counter
        self.folder_counter = self.FOLDER_INIT

        # Video enconder
        self.format = "FMP4"
        self.fourcc = VideoWriter_fourcc(*self.format)
        self.vid = None
        self.size = None
        self.outvid = os.path.join("/media/alejandro/DATA/datasets/simple_uav_dataset/real/", "out.mp4")
        # Call the subscriber
        self.image_subcriber()

        # Check time
        self.start = time.time()


    def image_callback(self, data):
        try:
            img = self.bridge.imgmsg_to_cv2(data, "bgr8")

            # Flip image horizontally
            img = cv2.flip(img, 0)

            # Write to video
            if self.vid is None:
                if self.size is None:
                    self.size = img.shape[1], img.shape[0]
                self.vid = VideoWriter(self.outvid, self.fourcc, float(self.FPS), self.size, True)
            if self.size[0] != img.shape[1] and self.size[1] != img.shape[0]:
                img = resize(img, self.size)
            self.vid.write(img)
        except CvBridgeError as e:
            print(e)




    def image_subcriber(self):
        # In ROS, nodes are uniquely named. If two nodes with the same
        # node are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        rospy.init_node('image_subcriber', anonymous=True)

        rospy.Subscriber(self.IMAGE_TOPIC, Image, self.image_callback)

        rospy.spin()

if __name__ == '__main__':
    ImageFromRosbag()