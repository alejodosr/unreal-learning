import numpy as np
import cv2
import os
from random import randint
import re

# Rootdir of the dataset
ROOTDIR = "/media/alejandro/TOSHIBA EXT/Insulators/test/images"
OUTDIR = "/media/alejandro/DATA/datasets/isolators_augmented_256_test/testA"
FINAL_RESOLUTION = 256  # Final square resolution of the unwrapped images
IMAGES_PER_FOLDER = 300   # Images per folder that gets unwrapped
DISABLE_RANDOM = True   # If disabled, it does not randomize the reading of files
DISABLE_CONSECUTIVE_COUNTING = True  # If disabled, it does not alert about not consecutive file names

counter = 0
last_jpg = False
last_number = -1

for subdir, dirs, files in os.walk(ROOTDIR):
    for file in sorted(files):
        print("Raw read: " + os.path.join(subdir, file))

        if file.endswith(".jpg") or file.endswith(".JPG") or file.endswith(".png"):
            if last_jpg is False:
                # Count files
                no_images = len([name for name in os.listdir(subdir) if os.path.isfile(os.path.join(subdir, name))])
                print("Number of images in folder: " + str(no_images))
                # Restart counter
                unwrapped_images = IMAGES_PER_FOLDER
                # Select images number
                images_to_save = []
                for i in range(IMAGES_PER_FOLDER):
                    images_to_save.append(randint(1, no_images))

                # Sort and remove duplicates
                images_to_save = list(set(images_to_save))
                images_to_save.sort()

                for i in range(len(images_to_save)):
                    print(images_to_save[i])

                unwrapped_images = len(images_to_save)

                # Restart variable
                last_number = -1

                #input()

            # Activate flag
            last_jpg = True

            # Load an color image in grayscale
            img = cv2.imread(os.path.join(subdir, file))

            # This assures that the files are erad in order
            result = re.findall("\d+", file)
            result = int(result[0])
            if (not DISABLE_CONSECUTIVE_COUNTING):
                if (result - last_number != 1):
                    print("WRONG READ! (" + str(last_number) + " different from " + str(result) + ")")
                    input()

            # Update variable
            last_number = result

            counter += 1
            print("Image number (" + str(counter) + ")")
            if DISABLE_RANDOM:
                resized_image = cv2.resize(img, (FINAL_RESOLUTION, FINAL_RESOLUTION))

                # Save image
                # cv2.imwrite(OUTDIR + "/" + str(counter).zfill(7) + ".jpg", resized_image)
                cv2.imwrite(OUTDIR + "/" + file + ".jpg", resized_image)
            else:

                if unwrapped_images > 0:
                    #print(str(images_to_save[unwrapped_images - 1]) + "     " + str(no_images))
                    if images_to_save[unwrapped_images - 1] == no_images:
                        resized_image = cv2.resize(img, (FINAL_RESOLUTION, FINAL_RESOLUTION))

                        # Save image
                        cv2.imwrite(OUTDIR + "/" + str(counter).zfill(7) + ".jpg", resized_image)

                        # Decrease number
                        unwrapped_images = unwrapped_images - 1
                        print("UNWRAPPED IMAGES VALUE: " + str(unwrapped_images))

                        # Show image

                        # cv2.imshow('image', img)
                        # cv2.imshow('image_resized', resized_image)
                        # cv2.waitKey(0)

            # Decrease pointer
            no_images = no_images - 1
        else:
            last_jpg = False

# Print final number of wrong images
print("FINAL NUMBER OF IMAGES: " + str(counter))


