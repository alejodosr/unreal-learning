import rospy
import cv2
# from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import tf
import os
from cv2 import VideoWriter, VideoWriter_fourcc, imread, resize
import time

class ImageFromRosbag(object):
    def __init__(self):

        self.IMAGE_TOPIC = "/SQ01/camera/color/image_raw"
        self.FOLDER_INIT = 0
        self.BASE_FOLDER = "/media/alejandro/DATA/datasets/simple_uav_dataset/real/"
        self.SHOW_IMAGE = False

        # Read tf
        self.listener = tf.TransformListener()

        self.bridge = CvBridge()

        # Frame counter
        self.frame_counter = 0

        try:
            if not os.path.exists(self.BASE_FOLDER + "save"):
                os.makedirs(self.BASE_FOLDER + "save")
        except OSError:
            print ('Error: Creating directory of data')

        # Call the subscriber
        self.image_subcriber()


    def image_callback(self, data):
        try:
            img = self.bridge.imgmsg_to_cv2(data, "bgr8")

            # Flip image horizontally
            img = cv2.flip(img, 0)

            cv2.imwrite(self.BASE_FOLDER + "save/" + str(self.frame_counter).zfill(6)+ ".jpg", img)

            # Increase counter
            self.frame_counter += 1

        except CvBridgeError as e:
            print(e)




    def image_subcriber(self):
        # In ROS, nodes are uniquely named. If two nodes with the same
        # node are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        rospy.init_node('image_subcriber', anonymous=True)

        rospy.Subscriber(self.IMAGE_TOPIC, Image, self.image_callback)

        rospy.spin()

if __name__ == '__main__':
    ImageFromRosbag()