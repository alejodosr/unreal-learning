import numpy as np
#import cv2
import os
from PIL import Image

# Rootdir of the dataset
rootdir = "/media/alejo/DATA/Alejo/datasets/simdronet_fake_256/collision_dataset/testing"
counter = 0
low_size = 256, 256
high_size = 960, 720

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        print(os.path.join(subdir, file))
        if file.endswith(".jpg"):
            # Load an color image in grayscale
            im = Image.open(os.path.join(subdir, file))

            counter += 1
            print("Image number (" + str(counter) + ")")

            # Reduce
            im2 = im.resize(low_size, Image.BICUBIC)

            # Aument
            im3 = im2.resize(high_size, Image.BICUBIC)

            # Save image
            im3.save(os.path.join(subdir, file))

# Print final number of wrong images
print("FINAL NUMBER OF IMAGES: " + str(counter))


