import rospy
import cv2
# from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import tf
import os
from cv2 import VideoWriter, VideoWriter_fourcc, imread, resize
import time
import glob
import os

IMAGE_TOPIC = "/SQ01/camera/color/image_raw"
FOLDER_INIT = 0
BASE_FOLDER = "/media/alejandro/DATA/datasets/results/m100_uav/"
FOLDER = "uav_inspection"
SHOW_IMAGE = False
FPS = 30
OUTVID = os.path.join(BASE_FOLDER, "out.mp4")


def make_video(outvid, images=None, fps=30, size=None,
               is_color=True, format="FMP4"):
    """
    Create a video from a list of images.

    @param      outvid      output video
    @param      images      list of images to use in the video
    @param      fps         frame per second
    @param      size        size of each frame
    @param      is_color    color
    @param      format      see http://www.fourcc.org/codecs.php
    @return                 see http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html

    The function relies on http://opencv-python-tutroals.readthedocs.org/en/latest/.
    By default, the video will have the size of the first image.
    It will resize every image to this size before adding them to the video.
    """
    from cv2 import VideoWriter, VideoWriter_fourcc, imread, resize
    fourcc = VideoWriter_fourcc(*format)
    vid = None
    for image in images:
        image = image.replace(FOLDER + '/', FOLDER + '/frame_')
        if not os.path.exists(image):
            raise FileNotFoundError(image)
        img = imread(image)
        if vid is None:
            if size is None:
                size = img.shape[1], img.shape[0]
            vid = VideoWriter(OUTVID, fourcc, float(fps), size, is_color)
        if size[0] != img.shape[1] and size[1] != img.shape[0]:
            img = resize(img, size)
        vid.write(img)
    vid.release()
    return vid

# Directory of images to run detection on
VIDEO_DIR = os.path.join(BASE_FOLDER, FOLDER)
images = list(glob.iglob(os.path.join(VIDEO_DIR, '*.*')))

# Use it if you have to remove a substring
for index, (image) in enumerate(images):
    images[index] = images[index].replace('frame_', '')

# Sort the images by integer index
images = sorted(images, key=lambda x: float(os.path.split(x)[1][:-3]))

make_video(OUTVID, images, fps=FPS)